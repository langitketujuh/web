---
title: "Pembayaran"
# meta description
description: "Jenis pembayaran"
image: images/thumbnail.jpg
opengraph:
  image: images/thumbnail.jpg
draft: false
---

<div class="row text-md-left">
  <div class="col-lg-12">
    <a href="https://www.paypal.com/paypalme/hervyqa" target="_blank" class="btn btn-lg btn-outline-primary">PayPal</a>
    <a href="https://liberapay.com/langitketujuh" target="_blank" class="btn btn-lg btn-outline-primary disabled">Liberapay</a>
    <a href="https://opencollective.com/langitketujuh" target="_blank" class="btn btn-lg btn-outline-primary disabled">OpenCollective</a>
    <p>Liberapay dan OpenCollective mengirim uang melalui <b>stripe</b> (kartu kredit), <a href="https://support.stripe.com/questions/requirements-to-open-a-stripe-account-in-indonesia">masih belum didukung di negara kami.</a>
    </p>
  </div>
</div>

## Bank transfer

- Pemilik akun bank   : `Hervy Qurrotul Ainur Rozi`
- Nama Bank           : PT. Bank Syariah Indonesia, tbk
- Nomer Rekening      : `7150725105`
- Kode Bank           : `4510017`
- Alamat              : RT02/02 Kembangsri, Ngoro
- Kota                : Mojokerto
- Negara              : ID (indonesia)
- Kode Pos            : `61385`

