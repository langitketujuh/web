---
title: "Catatan Rilis Langitketujuh OS 20250101"
date: 2024-12-31T13:55:26+08:00
image: images/blog/langitketujuh-os-20250101-release-notes.jpg
opengraph:
  image: images/blog/langitketujuh-os-20250101-release-notes.jpg
# post type (regular/featured)
type: "featured"
description: ""
author: "LangitKetujuh"
categories:
  - release
  - linux
tag:
  - linux
  - notes
draft: false
---

[LangitKetujuh] atau `L7OS` adalah distribusi GNU/Linux untuk desainer, ilustrator, animator, penerbitan, fotografer, arsitek, font kreator,
sinemator, game desainer dan untuk pengguna yang masih baru mengenal GNU/Linux.
Distribusi ini berdasarkan dari [Voidlinux] yang menggunakan [Runit], [KDE Plasma] lingkungan desktop,
model pembaruan stabil [Rilis bergulir] dan terdapat pustaka C yang efisien dengan [Musl libc].

Alhamdulillah kami dapat merilis LangitKetujuh dengan pembenahan dan fitur baru.
Berikut ini daftar pengumuman, perubahan dan catatan rilis pada versi `20250101`.

## Versi Studio dapat diunduh secara bebas

Ini salah satu berita yang gembira..

LangitKetujuh OS terdiri dari 2 edisi, yakni edisi **Home** dengan aplikasi standar dan edisi **Studio** dengan aplikasi yang lebih lengkap.
Di Edisi Home di versi sebelumnya dapat diunduh bebas, tetapi tidak di versi Studio.
Tetapi di versi **20250101** ini.
Kami memberikan akses penuh untuk mengunduh versi **Studio**.
Alasannya sederhana: **agar LangitKetujuh lebih bermanfaat untuk semua kalangan**.

Khusus pengguna untuk transaksi periode **november-desember 2024 akan otomatis dikembalikan dananya 100%**.
Silakan menghubungi **[@langitketujuh_cs]** via Telegram.

## Kompatibilitas CPU dan GPU

LangitKetujuh OS menyediakan 2 variasi merek CPU yaitu **Intel** dan **AMD**.
Serta ada dukungan GPU **Nouveau** dan **Nvidia** Proprietary driver.
Sehingga memungkinkan pengguna untuk memasang sesuai dengan mesin CPU dan GPU yang dimiliki.

Sederhananya variasi tersebut seperti ini:

| x86_64           | x86_64-musl       |
| :--------------- | ----------------- |
| iso amd          | iso amd           |
| iso intel        | iso intel         |
| iso amd-nvidia   | iso amd-nouveau   |
| iso intel-nvidia | iso intel-nouveau |

Dikarenakan versi **x86_64-musl** tidak ada Nvidia, maka menggunakan **Nouveau** driver.
Ada 8 variasi, kemudian dikalikan 2 edisi (Home dan Studio), total menjadi 16 variasi berkas `.iso` dengan ukuran sekitar **80GB** yang disimpan di *object storage* Jakarta.

| Ukuran | Nama berkas iso                                                  |
| :----- | ---------------------------------------------------------------- |
| 4.4G   | langitketujuh-live-x86_64-20250101-home-amd-nvidia.iso           |
| 3.9G   | langitketujuh-live-x86_64-20250101-home-amd.iso                  |
| 4.4G   | langitketujuh-live-x86_64-20250101-home-intel-nvidia.iso         |
| 3.9G   | langitketujuh-live-x86_64-20250101-home-intel.iso                |
| 6.3G   | langitketujuh-live-x86_64-20250101-studio-amd-nvidia.iso         |
| 5.8G   | langitketujuh-live-x86_64-20250101-studio-amd.iso                |
| 6.3G   | langitketujuh-live-x86_64-20250101-studio-intel-nvidia.iso       |
| 5.8G   | langitketujuh-live-x86_64-20250101-studio-intel.iso              |
| 3.9G   | langitketujuh-live-x86_64-musl-20250101-home-amd-nouveau.iso     |
| 3.9G   | langitketujuh-live-x86_64-musl-20250101-home-amd.iso             |
| 3.9G   | langitketujuh-live-x86_64-musl-20250101-home-intel-nouveau.iso   |
| 3.9G   | langitketujuh-live-x86_64-musl-20250101-home-intel.iso           |
| 5.8G   | langitketujuh-live-x86_64-musl-20250101-studio-amd-nouveau.iso   |
| 5.8G   | langitketujuh-live-x86_64-musl-20250101-studio-amd.iso           |
| 5.8G   | langitketujuh-live-x86_64-musl-20250101-studio-intel-nouveau.iso |
| 5.8G   | langitketujuh-live-x86_64-musl-20250101-studio-intel.iso         |
| 79G    | Total                                                            |

## Variasi tata letak tema

Kami pernah merilis tema tata-letak (layout) yang berbeda pada rilis sebelumnya, namun dihapus karena masalah Kompatibilitas QT6.
Pada rilis ini, kami membuat ulang tema dock-panel **LangitKetujuh Plus** dan **LangitKetujuh Unity**.
Tampilannya seperti berikut.

![Layout themes - LangitKetujuh OS](/images/blog/layout-themes-langitketujuh.webp)

Keduanya sedikit berbeda dari versi sebelumnya karena tidak menggunakan banyak widget di panel dan dock.

## Tombol *Titlebar* rata kiri

Secara bawaan tema LangitKetujuh menggunakan dekorasi jendela dengan tombol rata kanan.
Namun, untuk mendekatkan jarak ke *launcher menu* maka tombol *titlebar* diletakkan di sebelah kiri.

![Titlebar left button - LangitKetujuh OS](/images/blog/titlebar-left-button-langitketujuh.webp)

## Kzones utilitas

[Kzones] Fitur ini akan membantu pengguna agar lebih mudah dalam menata tata letak jendela.
Dengan dukungan fitur ini, pengguna jadi lebih cepat untuk membagi antar jendela aplikasi.

![KZones - LangitKetujuh OS](/images/blog/kzones-langitketujuh-os.webp)

## Sky pengganti L7-tools

L7-tools merupakan perkakas sederhana untuk upgrade dan chroot pengguna.
Sekarang L7-tools dengan perintah terminal `l7-tools` diganti dengan `sky` agar lebih sederhana.

![Sky L7-tools - LangitKetujuh OS](/images/blog/sky-langitketujuh-os.webp)

Sebagai informasi saja, sebenarnya perintah `upgrade` merupakan alias dari `sky --upgrade`.

## Telegram dan Firefox browser

Beberapa aplikasi tidak menggunakan dekorasi jendela bawaan, alias mebawa dekorasi jendela sendiri seperti telegram dan firefox.
Agar lebih universal dengan aplikasi lainnya, telegram dan firefox sudah dikonfigurasi untuk menggunakan dekorasi jendela milik KDE plasma.

## Google Noto CJK fonta

Menambahkan paket fonta untuk dukungan huruf Tradisional China, Jepang dan Korea.
Google Noto CJK fonta dirancang untuk memastikan bahwa teks dalam bahasa ini ditampilkan dengan benar tanpa karakter yang hilang atau terganti.
Terlebih lagi fonta Noto dirilis dengan lisensi bebas.

## Kernel LTS dihapus

Secara teknis, jika menggunakan dual kernel maka semakin besar ukurannya isonya.
Hal ini dikarenakan kompilasi modul DKMS di kernel juga semakin besar.
Alasan yang kedua, tujuannya untuk merampingkan penyimpanan daring kami.

## Perbedaan rilis iso

Pastinya dukungan driver tidak bloat yang mana **CPU** **intel** tidak perlu dependensi **AMD**, dan sebaliknya.
Varian AMD tentunya sudah ada **Radeon Profile** dengan servis _runit_ di latar belakang.
Jika menggunakan GPU Nvidia tentu paket driver Nvidia dimasukkan ke dalam versi tersebut.

---

## Terima kasih

Kepada semua pengguna, [pendukung, dan donatur](../../supporter) yang telah mendukung projek LangitKetujuh hingga saat ini,
kami sebagai tim LangitKetujuh mengucapkan terima kasih sebesar-besarnya atas dukungan dan kontribusinya yang sudah diberikan.

Jika belum bergabung di komunitas LangitKetujuh (indonesia)? silakan bergabung di [@langitketujuh_os] melalui telegram dan kanal [@langitketujuh_id].
Ikuti juga sosial media lainnya di [follow.langitketujuh.id].

[LangitKetujuh]:https://langitketujuh.id
[Voidlinux]:https://voidlinux.org
[Runit]:http://smarden.org/runit
[KDE Plasma]:https://kde.org/plasma-desktop
[Rilis bergulir]:https://id.wikipedia.org/wiki/Rilis_bergulir
[Musl libc]:https://www.musl-libc.org
[Kzones]:https://github.com/gerritdevriese/kzones

[@langitketujuh_cs]:https://t.me/langitketujuh_cs
[@langitketujuh_id]:https://t.me/langitketujuh_id
[@langitketujuh_os]:https://t.me/langitketujuh_os
[follow.langitketujuh.id]:https://follow.langitketujuh.id
