---
title: "Home x86_64-musl"
subtitle: ""
# meta description
type: "os/download/home/x86_64-musl"
description: "Terbaik untuk kebutuhan standar"
image: images/thumbnail.jpg
opengraph:
  image: images/thumbnail.jpg
draft: false
download:
  enable: true
  content: "Home edition for standard needs. There are firefox, inkscape, gimp, libreoffice, audio and video codecs. The musl architecture doesn't support non-free apps like nvidia, zoom, discord and others. But some application can be installed via flatpak.<br><br>
  LangitKetujuh OS Home edition is free of cost and doesn’t generate any direct sort of income. It is funded by advertising, sponsoring and donations and although it is financially supported by its own community of users. Support us to keep growing."
  button:
    enable: true
    label_amd: AMD
    link_amd: https://is3.cloudhost.id/langitketujuh/iso/langitketujuh-live-x86_64-musl-20250101-home-amd.iso
    label_intel: INTEL
    link_intel: https://is3.cloudhost.id/langitketujuh/iso/langitketujuh-live-x86_64-musl-20250101-home-intel.iso
    label_amd_nvidia: AMD+NOUVEAU
    link_amd_nvidia: https://is3.cloudhost.id/langitketujuh/iso/langitketujuh-live-x86_64-musl-20250101-home-amd-nouveau.iso
    label_intel_nvidia: INTEL+NOUVEAU
    link_intel_nvidia: https://is3.cloudhost.id/langitketujuh/iso/langitketujuh-live-x86_64-musl-20250101-home-intel-nouveau.iso
    sha256sum: https://is3.cloudhost.id/langitketujuh/iso/sha256sum.txt
  mirror:
    enable: false
    label_mirror_auto: Mirror Auto
    link_mirror_auto:
    label_mirror_1: Mirror 1
    link_mirror_1:
    label_mirror_2: Mirror 2
    link_mirror_2:
---
