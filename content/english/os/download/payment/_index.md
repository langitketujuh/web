---
title: "Payment"
# meta description
description: "Type of payment"
image: images/thumbnail.jpg
opengraph:
  image: images/thumbnail.jpg
draft: false
---

<div class="row text-md-left">
  <div class="col-lg-12">
    <a href="https://www.paypal.com/paypalme/hervyqa" target="_blank" class="btn btn-lg btn-outline-primary">PayPal</a>
    <a href="https://liberapay.com/langitketujuh" target="_blank" class="btn btn-lg btn-outline-primary disabled">Liberapay</a>
    <a href="https://opencollective.com/langitketujuh" target="_blank" class="btn btn-lg btn-outline-primary disabled">OpenCollective</a>
    <p>Liberapay and OpenCollective send money via <b>stripe</b> (credit card), <a href="https://support.stripe.com/questions/requirements-to-open-a-stripe-account-in-indonesia">still not supported in our country.</a>
    </p>
  </div>
</div>

## Bank transfer

- Account Holder Name : `Hervy Qurrotul Ainur Rozi`
- Bank name           : PT. Bank Syariah Indonesia, tbk
- Account Number      : `7150725105`
- Bank Code           : `4510017`
- First Line          : RT02/02 Kembangsri, Ngoro
- City                : Mojokerto
- Country             : ID (indonesia)
- Post Code           : `61385`

